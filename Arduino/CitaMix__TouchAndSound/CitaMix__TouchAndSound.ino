#include <FastCapacitiveSensor.h>
/*
   Capacitive touch sensors, made for CITAMIX event.
   Wiring:
   A0 -> 10M -> PAD
             |
           A1-5 (sensor)

   Smoothing value then sending to serial port to processing sketch

*/

FastCapacitiveSensor sensor1;
FastCapacitiveSensor sensor2;
FastCapacitiveSensor sensor3;
FastCapacitiveSensor sensor4;
FastCapacitiveSensor sensor5;

boolean buttonState[5] = {LOW, LOW, LOW, LOW, LOW};
boolean previousButtonState[5] = {LOW, LOW, LOW, LOW, LOW};
int score[5] = {0, 0, 0, 0, 0};

// Adust, lower is more sensitive
int seuil = 130;
int trig = 500;

FastCapacitiveSensor arrayOfSensors [5];


void setup()
{
  pinMode(A0, OUTPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);

  sensor1.begin(A0, A1, 5.0, 10, 10, 0.2);
  sensor2.begin(A0, A2, 5.0, 10, 10, 0.2);
  sensor3.begin(A0, A3, 5.0, 10, 10, 0.2);
  sensor4.begin(A0, A4, 5.0, 10, 10, 0.2);
  sensor5.begin(A0, A5, 5.0, 10, 10, 0.2);

  arrayOfSensors[0] = sensor1;
  arrayOfSensors[1] = sensor2;
  arrayOfSensors[2] = sensor3;
  arrayOfSensors[3] = sensor4;
  arrayOfSensors[4] = sensor5;

  Serial.begin(9600);

  establishContact();  // send a byte to establish contact until receiver responds

}

void establishContact() {
  while (Serial.available() <= 0) {
    Serial.println('A');   // send a capital A
    delay(300);
  }
}

void loop()
{
  if (Serial.available() > 0) {
    // Grab values
    for (int i = 0; i < 5; i++)
    {
      int readedSensor = arrayOfSensors[i].touch();
       //Serial.print(readedSensor);
       //Serial.print('\t');
      if (readedSensor > seuil) // somebody touched
      {
        if (score[i]<1000)score[i] += 500;

      }
    }

    //Smoothing values
    for (int i = 0; i < 5; i++)
    {
      score[i] = (2 * score[i]) / 3;
      //Serial.print(score[i]);
      //Serial.print('\t');
    }
    //Serial.println();
    
    // Set Button State
    for (int i = 0; i < 5; i++)
    {
      if ( score[i] > trig)// Front montant
      {
        if (buttonState[i] == LOW)
        {
          buttonState[i] = HIGH;
          previousButtonState[i] = LOW;
        }
      }
      else if ( score[i] < trig)// Front descendant
      {
        if  ( buttonState[i] == HIGH)
        {
          buttonState[i] = LOW;
          previousButtonState[i] = HIGH;
        }
      }
      // Sending to processing only if state has changed
      if (buttonState[i] != previousButtonState[i])
      {
        Serial.print(i);
        Serial.print(",");
        Serial.print(buttonState[i], DEC);
        Serial.print(",");
        Serial.println();
        previousButtonState[i] = buttonState[i];
      }
    }
  }
  delay(10);
}
