import processing.serial.*;
import ddf.minim.*;

Serial myPort;                  // Le port série

short LF = 10; // "Fin de ligne"

char HEADER_0 = '0'; 
char HEADER_1 = '1'; 
char HEADER_2 = '2'; 
char HEADER_3 = '3'; 
char HEADER_4 = '4'; 

Minim minim;

AudioPlayer audio0;
AudioPlayer audio1;
AudioPlayer audio2;
AudioPlayer audio3;
AudioPlayer audio4;

boolean firstContact = false;  

boolean[] buttonState = {false, false, false, false, false};
int offset;

void setup()
{
  size(512, 200, P3D);
  minim = new Minim(this);

  // load BD.wav from the data folder
  audio0 = minim.loadFile( "Sample1.wav", 512);
  audio1 = minim.loadFile( "Sample2.wav", 512);
  audio2 = minim.loadFile( "Sample3.wav", 512);
  audio3 = minim.loadFile( "Sample4.wav", 512);
  audio4 = minim.loadFile( "Sample5.wav", 512);

  // Nous imprimons la liste des ports série disponibles
  printArray(Serial.list());
  // Puis nous allons choisir le premier du tableau (à adapter suivant votre configuration), avant de nous connecter à 9600 baud (même vitesse que l'Arduino).
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
  
  offset = width/5;
  ellipseMode(CENTER);
  
}

void draw()
{
  background(0);
  stroke(255);
  
  //Some visual helpers
  for (int i = 0; i<5; i++)
  {
    if (buttonState[i] == true)
    {
      fill(255,0,0);
    } else {
      fill(20,20,20);
    }
    ellipse(offset/2+offset*i, height/2, 40,40);
  }
  
}

void serialEvent(Serial myPort) {
  String message = myPort.readStringUntil(LF); // On lit le message reçu, jusqu'au saut de ligne
  if (message != null)
  {
    print("Message " + message);
    // On découpe le message à chaque virgule, on le stocke dans un tableau
    String [] data = message.split(",");

    if (firstContact == false) {
      if (data[0].charAt(0) == 'A') {
        myPort.clear();          // clear the serial port buffer
        firstContact = true;     // you've had first contact from the microcontroller
        myPort.write('A');       // ask for more
      }
    } else {


      if (data[0].charAt(0) == HEADER_0)
      {
        // On convertit la valeur (String -> Int)
        int Btn1 = Integer.parseInt(data[1]);
        if (Btn1 == 1 && audio0.isPlaying() == false)
        {
          stopAll();
          audio0.rewind();
          audio0.play();
          buttonState[0] = true;
        }
      } else if (data[0].charAt(0) == HEADER_1)
      {
        // On convertit la valeur (String -> Int)
        int Btn2 = Integer.parseInt(data[1]);
        if (Btn2 == 1 && audio1.isPlaying() == false)
        {
          stopAll();
          audio1.rewind();
          audio1.play();
          buttonState[1] = true;
        }
      } else if (data[0].charAt(0) == HEADER_2)
      {
        // On convertit la valeur (String -> Int)
        int Btn3 = Integer.parseInt(data[1]);
        if (Btn3 == 1 && audio2.isPlaying() == false)
        {
          stopAll();
          audio2.rewind();
          audio2.play();
          buttonState[2] = true;
        }
      } else if (data[0].charAt(0) == HEADER_3)
      {
        // On convertit la valeur (String -> Int)
        int Btn4 = Integer.parseInt(data[1]);
        if (Btn4 == 1 && audio3.isPlaying() == false)
        {
          stopAll();
          audio3.rewind();
          audio3.play();
          buttonState[3] = true;
        }
      } else if (data[0].charAt(0) == HEADER_4)
      {
        // On convertit la valeur (String -> Int)
        int Btn5 = Integer.parseInt(data[1]);
        if (Btn5 == 1 && audio4.isPlaying() == false)
        {
          stopAll();
          audio4.rewind();
          audio4.play();
          buttonState[4] = true;
        }
      }
    }
  }
}

void stopAll() {
  audio0.pause();
  audio1.pause();
  audio2.pause();
  audio3.pause();
  audio4.pause();
  for (int i =0; i<5; i++)
  {
     buttonState[i] = false;
  }
}

void keyPressed() 
{
  if ( key == 'a' ) audio0.play();
  if ( key == 'z' ) audio1.play();
  if ( key == 'e' ) audio2.play();
  if ( key == 'r' ) audio3.play();
  if ( key == 't' ) audio4.play();
}
