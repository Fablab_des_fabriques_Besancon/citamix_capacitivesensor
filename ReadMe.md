**Programme réalisé dans le cadre de Citamix 2020**

-

Le projet consiste à créer 5 boutons capacitifs dans une maquette en polystyrène. 5 punaises sont reliées à 5 entrées analogiques d'un Arduino, reliées à la 6ème entrée analogique par une résistance de 10M.
Grâce à la bibliothèque FastCapacitiveSensor, nous détectons les contacts avec ces punaises, puis envoyons les changements d'état à un programme Processing. Celui gère la lecture de différentes pistes audio via la bibliothèque Minim.

Voir le dossier Pics pour une vidéo de démonstration.

